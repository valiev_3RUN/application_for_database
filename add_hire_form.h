#ifndef ADD_HIRE_FORM_H
#define ADD_HIRE_FORM_H

#include <QDialog>
#include <QString>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QMessageBox>
namespace Ui {
class Add_hire_form;
}

class Add_hire_form : public QDialog
{
    Q_OBJECT

public:
    explicit Add_hire_form(QWidget *parent = 0);
    void searchAutos();
    void searchClients();
    ~Add_hire_form();

private slots:

    void on_idAEdit_selectionChanged();

    void on_Add_hire_button_clicked();

    void on_idCEdit_selectionChanged();

    void on_tempTableView_doubleClicked(const QModelIndex &index);

    void on_lineEdit_textChanged(const QString &arg1);

    void on_lineEdit_2_textChanged(const QString &arg1);

    void on_tempTableView_clicked(const QModelIndex &index);

private:
    enum Current_table {Clients, Autos, Null} current_table;
    Ui::Add_hire_form *ui;
    QSqlQuery query;
    QSqlQueryModel qModel;
};

#endif // ADD_HIRE_FORM_H
