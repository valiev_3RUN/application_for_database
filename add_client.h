#ifndef ADD_CLIENT_H
#define ADD_CLIENT_H

#include <QDialog>
#include <QSqlQuery>
namespace Ui {
class Add_client;
}

class Add_client : public QDialog
{
    Q_OBJECT

public:
    explicit Add_client(QWidget *parent = 0);
    ~Add_client();

private slots:

    void on_addClientButton_clicked();

private:
    Ui::Add_client *ui;
    QSqlQuery query;
};

#endif // ADD_CLIENT_H
