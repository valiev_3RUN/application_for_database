#-------------------------------------------------
#
# Project created by QtCreator 2018-04-21T14:43:08
#
#-------------------------------------------------

QT       += core gui sql printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DB_Hire_Of_Autos
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        authorization.cpp \
    mainform.cpp \
    add_hire_form.cpp \
    add_client.cpp \
    add_fine_client.cpp \
    add_maintenance_form.cpp \
    add_auto_form.cpp \
    return_auto_form.cpp

HEADERS += \
        authorization.h \
    mainform.h \
    add_hire_form.h \
    add_maintenance_form.h \
    add_client.h \
    add_fine_client.h \
    add_auto_form.h \
    return_auto_form.h

FORMS += \
        authorization.ui \
    mainform.ui \
    add_hire_form.ui \
    add_maintenance_form.ui \
    add_client.ui \
    add_fine_client.ui \
    add_auto_form.ui \
    return_auto_form.ui
