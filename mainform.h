#ifndef MAINFORM_H
#define MAINFORM_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlQueryModel>
#include <QString>
#include <QKeyEvent>
#include <add_hire_form.h>
#include <add_maintenance_form.h>
#include <add_fine_client.h>
#include <add_client.h>
#include <add_auto_form.h>
#include <return_auto_form.h>
#include <QPrintDialog>
#include <QModelIndex>
namespace Ui {
class MainForm;
}

class MainForm : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainForm( QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL"), QWidget *parent = 0);
    QSqlDatabase& GetDataBase();
    void List_date_from_tables(QString current_table);
    void keyPressEvent(QKeyEvent* event);
    void Print(const QString& str);
    ~MainForm();
private slots:
    void on_singIn_triggered();
    void on_List_clients_triggered();
    void on_List_autos_triggered();
    void on_List_maintenanse_triggered();
    void on_List_hire_triggered();
    void on_List_fines_triggered();
    void on_List_fine_clients_triggered();
    void on_Add_info_button_clicked();
  void on_pushButton_clicked();

    void on_tableView_clicked(const QModelIndex &index);

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_ReturnAutoButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::MainForm *ui;
    QSqlDatabase db;
    QSqlTableModel tModel;
    QSqlQueryModel qModel;
    Add_hire_form* addHF;
    Add_maintenance_form* addMF;
    Add_fine_client* addFF;
    Add_client* addCF;
    add_auto_form* addAF;
    Return_Auto_form* retAutoF;
    QPrintDialog * prevDlg;
    QModelIndex index;
    QString id;
    enum Current_table {Clients, Autos, Maintenanse, Hire, Fines, Fine_clients, Null} current_table;
};

#endif // MAINFORM_H
