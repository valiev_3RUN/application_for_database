#include "authorization.h"
#include "ui_authorization.h"

Authorization::Authorization(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Authorization)
{
    ui->setupUi(this);
    Authorization::setFixedSize(270, 220);
}

Authorization::~Authorization()
{
    delete ui;
}

bool Authorization::isAuthorization() {
    QString login = ui->logIn->text();
    QString pass=  ui->passWord->text();
    for (int i = 0; i < login.length(); ++i) {
        for (int j = 0; j < pass.length(); ++j)  {
            if (login[i] == '*' || pass[j] == '*' ||
                login[i] == '/' || pass[j] == '/' ||
                login[i] == '-' || pass[j] == '-' ||
                login[i] == '.' || pass[j] == '.' ||
                login[i] == ',' || pass[j] == ','
               ) return false;
        }
    }
    return true;
}

/*Соединение*/
bool Authorization::Connection() {
    try
    {
        //QSqlDatabase* p =  QSqlDatabase::database("postgres");
        db = QSqlDatabase::addDatabase("QPSQL");
        db.setHostName("127.0.0.1");
        db.setDatabaseName("temp");
        db.setUserName(ui->logIn->text());
        db.setPassword(ui->passWord->text());
        db.setPort(5433);
        return db.open();

    } catch(...) {
        return false;
    }
}

/*Авторизация*/
void Authorization::on_pushButton_clicked()
{
 if (isAuthorization()) {
        if (!Connection()) {
            QMessageBox::information(this, "Ошибка соединения", db.lastError().text());

        } else {
            mainForm = new MainForm(db);
            mainForm->show();
            this->close();
        }

  } else {
        QMessageBox::information(this, "", "Символы *, /, -, ., запрещены" );
  }

}
