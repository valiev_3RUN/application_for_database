#ifndef ADD_FINE_CLIENT_H
#define ADD_FINE_CLIENT_H

#include <QDialog>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QSqlError>
#include <QString>
#include <QMessageBox>
namespace Ui {
class Add_fine_client;
}

class Add_fine_client : public QDialog
{
    Q_OBJECT

public:
    explicit Add_fine_client(QWidget *parent = 0);
    ~Add_fine_client();

private slots:
    void on_add_fine_client_button_clicked();

    void on_idHEdit_selectionChanged();

    void on_tempTableView_doubleClicked(const QModelIndex &index);

private:
    Ui::Add_fine_client *ui;
    QSqlQuery query;
    QSqlQueryModel qModel;
    enum Current_table {Hire, Fines, Null} current_table;
};

#endif // ADD_FINE_CLIENT_H
