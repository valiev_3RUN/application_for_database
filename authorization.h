#ifndef AUTHORIZATION_H
#define AUTHORIZATION_H

#include <QMainWindow>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <mainform.h>
#include <QMessageBox>
#include <QSqlError>
namespace Ui {
class Authorization;
}

class Authorization : public QMainWindow
{
    Q_OBJECT

public:
    explicit Authorization(QWidget *parent = 0);
    ~Authorization();
    bool Connection();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Authorization *ui;
    QSqlDatabase db;
    MainForm* mainForm;
    bool isAuthorization();
};

#endif // AUTHORIZATION_H
