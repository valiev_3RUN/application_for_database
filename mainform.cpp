#include "mainform.h"
#include "ui_mainform.h"
#include "authorization.h"
#include <QMessageBox>
#include <QString>
#include <QPrintDialog>
#include <QPrinterInfo>
#include <QPrinter>
#include <QPainter>
#include <QRect>
#include <QFont>
#include <QFileDialog>
MainForm::MainForm(QSqlDatabase db, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainForm)
{
    this->db = db;
    ui->setupUi(this);
    current_table = Null;
    ui->Add_info_button->hide();
    ui->pushButton->hide();
    ui->ReturnAutoButton->hide();
    ui->groupBox->hide();
}

void MainForm::Print(const QString &str) {
    QPrinter printer;
    printer.setPageSize(QPrinter::A4Small);
    prevDlg = new QPrintDialog(&printer);
    prevDlg->exec();
    QPrinterInfo pInfo(printer);
    printer.setPageSize(pInfo.maximumPhysicalPageSize());
    printer.setPageMargins(0,0,0,0,QPrinter::Millimeter);
    QPainter painter;
    painter.begin(&printer);
    QFont font("Monospace");
    font.setStyleHint(QFont::Monospace);
    painter.setFont(font);
    QRect rect = painter.boundingRect(0,0,5000,5000,Qt::AlignLeft|Qt::AlignTop, str);
    painter.scale(
    (double)printer.pageRect().width() / (double)rect.width(),
    (double)printer.pageRect().width() / (double)rect.width());
    painter.drawText(rect,str);
    painter.end();
}
/*Печать*/
void MainForm::on_pushButton_clicked()
{

    if (this->current_table == Hire) {
        QString infoClient[11];
        for (int i = 0; i < 11; ++i) {
            infoClient[i] = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),i)).toString();
        }
        QString printStr = "ФИО: " + infoClient[0] + "\t" + infoClient[1] + "\t" + infoClient[2]+ "\nПасспорт: " + infoClient[3] + "\n" +
                           "Марка машины: " + infoClient[4] + "\n" + "Модель: " + infoClient[5] + "\nРег. номер: " + infoClient[6] +
                           "\nДата аренды: " + infoClient[7] + "\nДата возврата: " + infoClient[8] + "\nФактическая дата возврата: " + infoClient[9] +
                           "\n\n\n\t\t\t\t\t\t\tСтоимость проката: " + infoClient[10] + " рублей 00 копеек";
        printStr = "\t\t\t\tСВЕДЕНИЯ О ПРОКАТЕ\n" + printStr;
        Print(printStr);

    }
    if (this->current_table == Fine_clients) {
        QString infoFineClients[13];
        for (int i = 0; i < 13; ++i) {
            infoFineClients[i] = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),i)).toString();
        }
        QString printStr = "ФИО: " + infoFineClients[0] + "\t" + infoFineClients[1] + "\t" + infoFineClients[2]+ "\nПасспорт: " + infoFineClients[3] + "\n" +
                           "Марка машины: " + infoFineClients[4] + "\n" + "Модель: " + infoFineClients[5] + "\n" + "Рег. номер: " + infoFineClients[6] + "\n" +
                           "Цена за 1 день: " + infoFineClients[7] + "\nДата аренды: " + infoFineClients[8] + "\nДата возврата: " + infoFineClients[9] + "\nФактич. дата возврата: " + infoFineClients[10] +
                           "\nПричина штрафа: " + infoFineClients[11] + "\n\n\n\n\t\t\t\t\tСтоимость штрафа: " + infoFineClients[12] +" рублей 00 копеек";
        printStr = "\t\t\t\tСВЕДЕНИЯ О Штрафе\n" + printStr;
        Print(printStr);
    }

}

MainForm::~MainForm()
{
    delete ui;
    delete prevDlg;
    delete addHF;
    delete addMF;
    delete addFF;
    delete addCF;
}

QSqlDatabase& MainForm::GetDataBase() {

    return db;

}




/*Войти с другого аккаунта*/
void MainForm::on_singIn_triggered()
{
    Authorization* form = new Authorization();
    form->show();
    this->close();
}


/*Вывод данных в таблицу*/
void MainForm::List_date_from_tables(QString current_table) {
      ui->Add_info_button->setVisible(true);
      ui->pushButton->hide();
      ui->groupBox->hide();
      ui->tableView->showColumn(0);
      ui->tableView->showColumn(6);
      ui->tableView->showColumn(8);
      ui->tableView->showColumn(11);
      switch (this->current_table) {
      case Clients:
          tModel.setTable(current_table);
          tModel.select();
          tModel.setHeaderData(0, Qt::Horizontal, "ID клиента");
          tModel.setHeaderData(1, Qt::Horizontal, "Фамилия");
          tModel.setHeaderData(2, Qt::Horizontal, "Имя");
          tModel.setHeaderData(3, Qt::Horizontal, "Отчество");
          tModel.setHeaderData(4, Qt::Horizontal, "Телефон");
          tModel.setHeaderData(5, Qt::Horizontal, "Пасспорт");
          tModel.setHeaderData(6, Qt::Horizontal, "Вод. удостоверение");
          ui->tableView->setModel(&tModel);
          ui->tableView->hideColumn(0);
          ui->Add_info_button->setText("Добавить клиента");
          //ui->pushButton->hide();
          break;
      case Autos :
          tModel.setTable(current_table);
          tModel.select();
          tModel.setHeaderData(0, Qt::Horizontal, "ID авто");
          tModel.setHeaderData(1, Qt::Horizontal, "Рег. номер");
          tModel.setHeaderData(2, Qt::Horizontal, "Марка");
          tModel.setHeaderData(3, Qt::Horizontal, "Модель");
          tModel.setHeaderData(4, Qt::Horizontal, "Цвет");
          tModel.setHeaderData(5, Qt::Horizontal, "Цена за сутки");
          tModel.setHeaderData(6, Qt::Horizontal, "Количество ТО");
          tModel.setHeaderData(7, Qt::Horizontal, "Статус(free/busy)");
          ui->tableView->setModel(&tModel);
          ui->tableView->hideColumn(0);
          ui->tableView->hideColumn(8);
          ui->Add_info_button->setText("Добавить автомобиль");
         // ui->pushButton->hide();

          break;
      case Hire :
         ui->tableView->setVisible(true);
         qModel.setQuery("select * from list_hire()");
         ui->tableView->setModel(&qModel);
         ui->Add_info_button->setText("Оформить прокат");
         ui->tableView->hideColumn(11);
       //  ui->pushButton->setVisible(true);
        // ui->pushButton->setText("Печать");
          break;
      case Maintenanse :
          ui->tableView->setVisible(true);
          qModel.setQuery("select * from list_maintenance()");
          ui->tableView->setModel(&qModel);
          ui->Add_info_button->setText("Добавить ТО");
          ui->tableView->hideColumn(6);
         // ui->pushButton->hide();
          break;
      case Fines :
           tModel.setTable(current_table);
           tModel.select();
           tModel.setHeaderData(0, Qt::Horizontal, "ID выписки штрафа");
           tModel.setHeaderData(1, Qt::Horizontal, "Причина штрафа");
           tModel.setHeaderData(2, Qt::Horizontal, "Стоимость штрафа");
           ui->tableView->setModel(&tModel);
           ui->tableView->hideColumn(0);
           ui->Add_info_button->setText("Добавить штраф");
          // ui->pushButton->hide();
          break;
      case Fine_clients :
           ui->tableView->setVisible(true);
           qModel.setQuery("Select * from list_fine_clients()");
           ui->tableView->setModel(&qModel);
           ui->Add_info_button->setText("Добавить штраф");
          // ui->pushButton->setVisible(true);
          // ui->pushButton->setText("Печать штрафа");
          break;
      }


}

/*Просмотр клиентов*/
void MainForm::on_List_clients_triggered()
{
    current_table = Clients;
    List_date_from_tables("clients");

}

/*Просмотр автомобилей*/
void MainForm::on_List_autos_triggered()
{
    current_table = Autos;
    List_date_from_tables("Autos");

}

/*Просмотр технического обслуживания*/
void MainForm::on_List_maintenanse_triggered()
{
    current_table = Maintenanse;
    List_date_from_tables("Maintenanse");   
}
/*Просмотр информации о прокате автомобилей*/
void MainForm::on_List_hire_triggered()
{
     current_table = Hire;
     List_date_from_tables("Hire");
}
/*Просмотр выписки штрафа*/
void MainForm::on_List_fines_triggered()
{ 
     current_table = Fines;
    // List_date_from_tables("Fines");
}
/*Просмотр оштрафованных клиентов*/
void MainForm::on_List_fine_clients_triggered()
{
     current_table = Fine_clients;
     List_date_from_tables("Fine_clients");
}



/*Удаление строки по кнопке Delete*/
void MainForm::keyPressEvent(QKeyEvent* event) {
    if (event->key() == Qt::Key_F5) {
        switch (current_table) {
        case Clients:
            List_date_from_tables("Clients");
            break;
        case Autos:
            List_date_from_tables("Autos");
            break;
        case Hire:
            List_date_from_tables("Hire");
            break;
        case Maintenanse:
            List_date_from_tables("Maintenanse");
            break;
        case Fines:
            List_date_from_tables("Fines");
            break;
        case Fine_clients:
            List_date_from_tables("Fine_clients");
            break;
        }
    }

    if (event->key() == Qt::Key_Delete && (current_table == Clients || current_table == Autos || current_table == Fines)) {
        int selected_row = ui->tableView->currentIndex().row();
        if (selected_row >= 0) {
            tModel.removeRow(selected_row);
            switch (current_table) {
            case Clients:
                List_date_from_tables("Clients");
                break;
            case Autos:
                List_date_from_tables("Autos");
                break;
            case Fines:
                List_date_from_tables("Fines");
                break;
            }
        }
    }

}



/*Добавление информации в таблицу*/
void MainForm::on_Add_info_button_clicked()
{  

    switch (this->current_table) {
    case Clients:
        //tModel.insertRow(tModel.rowCount());
        addCF = new Add_client();
        addCF->setModal(true);
        addCF->show();
        break;
    case Autos:
        //tModel.insertRow(tModel.rowCount());
        addAF = new add_auto_form();
        addAF->setModal(true);
        addAF->show();
        break;
    case Hire:
        addHF = new Add_hire_form(this);
        addHF->setModal(true);
        addHF->show();
        break;
    case Maintenanse:
        addMF = new Add_maintenance_form(this);
        addMF->setModal(true);
        addMF->show();
        break;
    case Fines:
       tModel.insertRow(tModel.rowCount());
        break;
    case Fine_clients:
        addFF = new Add_fine_client(this);
        addFF->setModal(true);
        addFF->show();
       break;
    }
}

void MainForm::on_tableView_clicked(const QModelIndex &index)
{
   // QMessageBox::information(0, "", QString::number(ui->tableView->currentIndex().column()));
    int i = ui->tableView->currentIndex().row();
    QString status = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),7)).toString();

    if (i >= 0 && (current_table == Autos || current_table == Maintenanse || current_table == Hire)) {
        ui->groupBox->show();
        QString path;
        switch (current_table) {
        case Autos:  path = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),8)).toString(); break;
        case Hire:   path = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),11)).toString();break;
        case Maintenanse: path = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),6)).toString(); break;
        default:
            break;
        }
        ui->label_2->setScaledContents(true);
        ui->label_2->setPixmap(path);
    } else
        ui->groupBox->hide();

    if (i >= 0 && current_table == Autos && status == "busy") {
        ui->ReturnAutoButton->setVisible(true);
        id = ui->tableView->model()->data(ui->tableView->model()->index(index.row(),0)).toString();
    } else
        ui->ReturnAutoButton->hide();
    if (i >= 0 && (current_table == Hire || current_table == Fine_clients)) {
        ui->pushButton->setText("Печать строки № - " + QString::number(i + 1));
        this->index = index;
        ui->pushButton->setVisible(true);
    } else
        ui->pushButton->hide();
}

void MainForm::on_tableView_doubleClicked(const QModelIndex &index)
{
   //ui->tableView->model()->data(ui->tableView->model()->index(index.row(),i)).toString();
    if (ui->tableView->currentIndex().column() == 7 && current_table == Autos){
          List_date_from_tables("Autos");
    };
}

void MainForm::on_ReturnAutoButton_clicked()
{
    retAutoF = new Return_Auto_form(id);
    retAutoF->setModal(true);
    retAutoF->show();
}


