#include "add_fine_client.h"
#include "ui_add_fine_client.h"

Add_fine_client::Add_fine_client(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Add_fine_client)
{
    ui->setupUi(this);
    ui->costLine->setValidator(new QIntValidator(0, 999999999, this));
}

Add_fine_client::~Add_fine_client()
{
    delete ui;
}

void Add_fine_client::on_add_fine_client_button_clicked()
{
    if (ui->idHEdit->text() != "Double click.." && ui->causeLine->text() !="" && ui->costLine->text() != "") {
        QString stringQuery = "INSERT INTO Fine_clients (id_hire, cause_fine, cost_fine) values(" + ui->idHEdit->text() + ",'" + ui->causeLine->text() + "'," + ui->costLine->text() +  ");";

        if (query.exec(stringQuery)) {
            QMessageBox::information(this, "Good", "Штраф для клиента добавлен");
            this->close();
        } else {
            QMessageBox::information(this, "Ошибка", query.lastError().text());
        }
    } else {
        QMessageBox::information(this,"Ошибка", "Заполните все поля");
    }
}

void Add_fine_client::on_idHEdit_selectionChanged()
{
    qModel.setQuery("SELECT Hire.id_hire as \"ID\", clients.last_name as \"Фамилия\", clients.first_name as \"Имя\", data_issue as \"Дата аренды\", actual_return as \"Факт. дата возврата\", total_cost as \"Общ. сто-сть\" From Hire INNER JOIN Clients on Clients.id_client = Hire.id_client;");
    ui->tempTableView->setModel(&qModel);
    this->current_table = Hire;
}

void Add_fine_client::on_tempTableView_doubleClicked(const QModelIndex &index)
{
    if (ui->tempTableView->currentIndex().row() >= 0) {
        QString ID;
        switch (current_table) {
        case Hire:
            ID = ui->tempTableView->model()->data(ui->tempTableView->model()->index(index.row(),0)).toString();
            ui->idHEdit->setText(ID);
            break;
        case Fines:
            ID = ui->tempTableView->model()->data(ui->tempTableView->model()->index(index.row(),0)).toString();
            //ui->idFEdit->setText(ID);
            break;
        }
    }
}
