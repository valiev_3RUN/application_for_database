#include "add_hire_form.h"
#include "ui_add_hire_form.h"
#include <QSqlError>

Add_hire_form::Add_hire_form(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Add_hire_form)
{
    current_table = Null;
    ui->setupUi(this);
}

Add_hire_form::~Add_hire_form()
{
    delete ui;
    ui->SerchLabel->hide();
    ui->SerchLabel_2->hide();
    ui->lineEdit->hide();
    ui->lineEdit_2->hide();
}




/*Добавление проката*/
void Add_hire_form::on_Add_hire_button_clicked()
{
    int day, month, year;
    ui->dateIssueCal->selectedDate().getDate(&day, &month, &year);
    QString date_issue = QString::number(year) + "-" + QString::number(month) + "-" + QString::number(day);
    ui->dateReturnCal->selectedDate().getDate(&day, &month, &year);
    QString date_return = QString::number(year) + "-" + QString::number(month) + "-" + QString::number(day);
    //ui->actuaReturnCal->selectedDate().getDate(&day, &month, &year);
    //QString actual_return = QString::number(year) + "-" + QString::number(month) + "-" + QString::number(day);
    QString stringQuery = "Select add_hire(" + ui->idAEdit->text() + "," + ui->idCEdit->text() + "," + "'" + date_issue + "','" + date_return + "');";
    if (ui->idCEdit->text() != "Double click.." && ui->idAEdit->text() != "Double click..") {
        if (query.exec(stringQuery)) {
            QMessageBox::information(this, "Good", "Прокат оформлен");
            this->close();
        } else {
            QMessageBox::information(this, "Ошибка","Проверьте корректность введенных данных!");

        }
    } else QMessageBox::information(this, "Ошибка добавления", "Заполните все поля");
}

/*Поле ввода для выбора клиента*/
void Add_hire_form::on_idCEdit_selectionChanged()
{
   ui->groupBox->hide();
   ui->lineEdit_2->hide();
   ui->SerchLabel_2->hide();
   ui->SerchLabel->setVisible(true);
   ui->lineEdit->setVisible(true);
   ui->SerchLabel->setText("Поиск по пасспорту: ");
   qModel.setQuery("select id_client as \"ID\", last_name as \"Фамилия\", first_name as \"Имя\", passport as \"Паспорт\" from Clients;");
   ui->tempTableView->setModel(&qModel);
   current_table = Clients;
}


/*Поле ввода для выбора авто*/
void Add_hire_form::on_idAEdit_selectionChanged()
{
    ui->groupBox->hide();
    ui->lineEdit->setVisible(true);
    ui->lineEdit_2->setVisible(true);
    ui->SerchLabel_2->setVisible(true);
    ui->SerchLabel->setVisible(true);
    ui->SerchLabel->setText("Поиск по марке: ");
    qModel.setQuery("Select id_auto as \"ID\", Num as \"Рег. номер\",  mark as \"Марка\","
                    " Model as \"Модель\", Color as \"Марка\", cost_hire_in_day as \"Цена за сутки\","
                    "Count_maintenance as \"Кол-во ТО\", status as \"Статус\", image from Autos where status = 'free';");
    ui->tempTableView->setModel(&qModel);
    current_table = Autos;
}

/*Выборка клиента/авто*/
void Add_hire_form::on_tempTableView_doubleClicked(const QModelIndex &index)
{
    if (ui->tempTableView->currentIndex().row() >= 0) {
        QString ID;
        switch (current_table) {
        case Autos:
            ID = ui->tempTableView->model()->data(ui->tempTableView->model()->index(index.row(),0)).toString();
            ui->idAEdit->setText(ID);
            break;
        case Clients:
            ID = ui->tempTableView->model()->data(ui->tempTableView->model()->index(index.row(),0)).toString();
            ui->idCEdit->setText(ID);
            break;
        }

    }
}

void Add_hire_form::searchClients() {
    if (ui->lineEdit->text() != "") {
        QString one = "select id_client as \"ID\", last_name as \"Фамилия\", first_name as \"Имя\", passport as \"Паспорт\" from Clients ";
        QString two = "where passport like '" + ui->lineEdit->text() + "%';";
        QString query = one + two;
        qModel.setQuery(query);
        ui->tempTableView->setModel(&qModel);

    } else {
        qModel.setQuery("select id_client as \"ID\", last_name as \"Фамилия\", first_name as \"Имя\", passport as \"Паспорт\" from Clients;");
        ui->tempTableView->setModel(&qModel);
    }
}

void Add_hire_form::searchAutos() {
    if (ui->lineEdit->text() != "" || ui->lineEdit_2->text() != "") {
        QString one = "Select id_auto as \"ID\",  Num as \"Рег. номер\", mark as \"Марка\", Model as \"Модель\",  Color as \"Марка\", cost_hire_in_day as \"Цена за сутки\",";
        QString two = "Count_maintenance as \"Кол-во ТО\", status as \"Статус\"from Autos where status = 'free'";
        QString three = " and mark like '" + ui->lineEdit->text() + "%' and model like '" + ui->lineEdit_2->text() + "%';";
        QString query = one + two + three;
        qModel.setQuery(query);
        ui->tempTableView->setModel(&qModel);
    } else {
        QString one = "Select id_auto as \"ID\", Num as \"Рег. номер\",mark as \"Марка\", Model as \"Модель\",   Color as \"Марка\", cost_hire_in_day as \"Цена за сутки\",";
        QString two = "Count_maintenance as \"Кол-во ТО\", status as \"Статус\"from Autos where status = 'free';";
        QString query = one + two;
        qModel.setQuery(query);
        ui->tempTableView->setModel(&qModel);
    }
}

void Add_hire_form::on_lineEdit_textChanged(const QString &arg1)
{
    switch (this->current_table) {
    case Autos:
         searchAutos();
        break;
    case Clients:
        searchClients();
        break;
    default:
        break;
    }
}

void Add_hire_form::on_lineEdit_2_textChanged(const QString &arg1)
{
    switch (this->current_table) {
    case Autos:
         searchAutos();
        break;
    case Clients:
        break;
    default:
        break;
    }
}



void Add_hire_form::on_tempTableView_clicked(const QModelIndex &index)
{
    int i = ui->tempTableView->currentIndex().column();
    //QMessageBox::information(0, "", QString::number(i));
    if (current_table == Autos && i >= 0) {
        ui->groupBox->show();
        QString  path = ui->tempTableView->model()->data(ui->tempTableView->model()->index(index.row(),8)).toString();
        ui->label_2->setScaledContents(true);
        ui->label_2->setPixmap(path);
    } else
        ui->groupBox->hide();
}
