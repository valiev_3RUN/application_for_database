#include "add_auto_form.h"
#include "ui_add_auto_form.h"
#include <QFileDialog>
add_auto_form::add_auto_form(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::add_auto_form)
{
    ui->setupUi(this);
    ui->CostLine->setValidator(new QIntValidator(0, 999999, this));
    ui->TOLine->setValidator(new QIntValidator(0, 50, this));
}

add_auto_form::~add_auto_form()
{
    delete ui;
}


void add_auto_form::on_addAutoButton_clicked()
{

    if (ui->NumAutoLine->text() != "" &&
        ui->MarkLine->text()    != "" &&
        ui->ModelLine->text() != "" &&
        ui->CostLine->text()    != "" &&
        ui->ColorLine->text()   != "")
    {
        QString strQuery = "INSERT INTO Autos (Num, mark, model, color, cost_hire_in_day, count_maintenance, image) VALUES ('" +
                           ui->NumAutoLine->text() + "','" +
                           ui->MarkLine->text()+ "','" + ui->ModelLine->text() + "','" +  ui->ColorLine->text() + "','" +
                           ui->CostLine->text() + "','" +   ui->TOLine->text() +"','"+ pathImage +"');";
      //  QMessageBox::information(0, "", strQuery);
        if (query.exec(strQuery)) {
            QMessageBox::information(0, "", "Запись добавлена");
            this->close();
        } else
            QMessageBox::information(this, "Ошибка добавления", "Проверьте корректность введенных данных");
    }  else
        QMessageBox::information(this, "", "Заполнинте все поля!");
}


void add_auto_form::on_toolButton_clicked()
{
    pathImage = QFileDialog::getOpenFileName(0, "Открыть", "./", "*");
    ui->label->setScaledContents(true);
    ui->label->setPixmap(pathImage);
}
