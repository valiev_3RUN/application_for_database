
/*Создание таблиц*/

Create type Gender as ENUM ('free', 'busy');

CREATE TABLE  Autos (
	id_auto SERIAL PRIMARY KEY UNIQUE NOT NULL,
	Num varchar(10) CHECK(Num != '') NOT NULL,
	Mark VARCHAR(15) CHECK(Model != '')  NOT NULL,
	Model VARCHAR(15) CHECK(Mark != '')  NOT NULL,
	Color VARCHAR(10) CHECK(Color != '')  NOT NULL,
	Cost_hire_in_day INT CHECK(cost_hire_in_day > 0)  NOT NULL,
	Count_maintenance INT CHECK (Count_maintenance >= 0)  NOT NULL,
	status Gender default 'free',
	image varchar
);
--insert into autos (model, mark, color, cost_hire_in_day, count_maintenance, status) values ('BMW', 'E36', 'Белый', 1500, 0, 'free');
/*Техническое обслуживание*/
CREATE TABLE Maintenance (
	id_TO SERIAL PRIMARY KEY UNIQUE NOT NULL,
	id_auto INT REFERENCES Autos(id_auto) ON DELETE  CASCADE ON UPDATE CASCADE CHECK(id_auto > 0) NOT NULL ,
	oil_level varchar(15) NOT NULL,
	brakes varchar (15) NOT NULL,
	date_maintenance date
);
/*Клиенты*/
CREATE TABLE Clients (
	id_client SERIAL PRIMARY KEY UNIQUE NOT NULL,
	first_name VARCHAR(20) CHECK(first_name != '')  NOT NULL,
	last_name VARCHAR(20) CHECK(last_name != '')  NOT NULL,
	third_name VARCHAR(20) CHECK(last_name != '')  NOT NULL, 
	telephone varchar (11) NOT NULL,
	passport varchar (9)  NOT NULL UNIQUE, 
	driver_license varchar (11) NOT NULL UNIQUE
);

/*Выдача на прокат*/
CREATE TABLE Hire (

	id_hire SERIAL PRIMARY KEY UNIQUE NOT NULL,
	id_auto INT REFERENCES Autos(id_auto) ON DELETE  CASCADE ON UPDATE CASCADE CHECK(id_auto > 0) NOT NULL ,
	id_client INT REFERENCES Clients(id_client) ON DELETE  CASCADE ON UPDATE CASCADE CHECK(id_client > 0) NOT NULL ,
	data_issue date CHECK (data_issue >= current_date),
	data_return date CHECK(data_return > data_issue),
	actual_return date CHECK (actual_return >= data_issue),
	total_cost int CHECK(total_cost > 0) NOT NULL
);


/*Оштрафованные клиенты*/
CREATE TABLE Fine_clients  (
	id_fine_clients SERIAL PRIMARY KEY UNIQUE NOT NULL,
	--id_fines INT REFERENCES Fines(id_fines) ON DELETE CASCADE ON UPDATE CASCADE CHECK(id_fines > 0) NOT NULL,
	id_hire  INT REFERENCES Hire(id_hire) ON DELETE CASCADE ON UPDATE CASCADE  CHECK(id_hire > 0) NOT NULL,
	cause_fine varchar(30) NOT NULL,
	cost_fine INT CHECK(cost_fine > 0)
);



/*Вывод технического обслуживания*/
CREATE OR REPLACE FUNCTION list_maintenance() returns table
( "Рег. номер" varchar(10), "Марка" varchar(15), "Модель" varchar(15), "Уровень масла" varchar(15), "Уровень тормозной жидкости" varchar(15), "Дата ТО" date, "image" varchar)
AS $$
	BEGIN
		return QUERY
		select Autos.Num,
			   Autos.mark,
			   Autos.model,
			   oil_level,
			   brakes,
			   date_maintenance,
			   Autos.image
		FROM Maintenance INNER JOIN Autos on Autos.id_auto = Maintenance.id_auto;
	END
$$ LANGUAGE plpgsql security definer;

/*Вывод таблицы штрафов*/
CREATE OR REPLACE FUNCTION list_fine_clients() returns table
("Фамилия" varchar(15), "Имя" varchar(15), "Отчество" varchar(15), "Пасспорт" varchar(9), "Марка" varchar(15), "Модель" varchar(15), "Рег. номер" varchar(20), "Цена за 1 день" int, "Дата аренды" date, "Дата возврата" date,
 "Фактич. дата возврата" date,  "Причина штрафа" varchar(30), "Цена штрафа" int)
AS $$
	BEGIN
		return QUERY
		select Clients.last_name,
			   clients.first_name,
			   Clients.third_name,
			   Clients.passport,
			   autos.mark,
			   autos.model,
			   autos.num,
			   autos.cost_hire_in_day,
			   Hire.data_issue,
			   Hire.data_return,
			   Hire.actual_return,
			   fine_clients.cause_fine,
			   fine_clients.cost_fine
		FROM Fine_clients
		INNER JOIN Hire on Hire.id_hire = Fine_clients.id_hire
		INNER JOIN Autos on Autos.id_auto = Hire.id_auto
		INNER JOIN Clients on Hire.id_client = Clients.id_client;		
			   
	END
$$ LANGUAGE plpgsql security definer;


/*Вывод проката */
CREATE FUNCTION List_Hire() returns
TABLE ("Фамилия" varchar(20), "Имя" varchar(20),"Отчество" varchar(15),"Пасспорт" varchar(9), "Марка" varchar(20), "Модель" varchar(20),"Рег. номер" varchar(20), "Дата аренды" date, "Дата возврата" date, "Фактич. дата воз-та" date, "Общ. стоимость" int, "imagePath" varchar)
AS $$
	BEGIN
	return QUERY
		select Clients.last_name,
			   clients.first_name,
			   Clients.third_name,
			   Clients.passport,
			   Autos.mark, Autos.model,
			   Autos.Num,
			   data_issue, data_return,
			   actual_return, total_cost,
			   Autos.image
		from Hire
		INNER JOIN clients on clients.id_client = Hire.id_client
		INNER JOIN Autos on Autos.id_auto = hire.id_auto;	
	END
$$ LANGUAGE plpgsql security definer; 




/*Вывод клиентов*/
CREATE  OR REPLACE FUNCTION List_client() returns table
(ID_клиента int, "Имя" varchar(15) , "Фамилия" varchar(15), "Отчество" varchar(15), "Телефон" varchar(11), "Паспорт" varchar(9), "ВУ" varchar(11))
AS $$
BEGIN
RETURN QUERY
		SELECT  id_client,
				first_name, 
				last_name,
				third_name,
				telephone ,
				passport,
				driver_license	
		FROM Clients;
END;
$$ LANGUAGE plpgsql security definer;



/*Определение общей стоимости проката (Промежуточная функция)*/
CREATE OR REPLACE FUNCTION Pricing(id_autoP int, data_issueP date,  data_returnP date) returns int
AS $$
DECLARE cost_hire int;
BEGIN
	SELECT cost_hire_in_day from Autos where id_auto = id_autoP into cost_hire;
	return (data_returnP - data_issueP) * cost_hire;
END;
$$ LANGUAGE plpgsql;


/*Возврат автомобиля*/
CREATE OR REPLACE FUNCTION Return_auto(actual_returnP date, id_autoP int) returns int
AS  $$
DECLARE id_hireP int;
DECLARE data_returnP date;
BEGIN
	if (actual_returnP) > (select data_return from Hire where id_auto = id_autoP and actual_return is null) then
		select id_hire from hire where id_auto = id_autoP and actual_return is null into id_hireP;
		select data_return from hire where id_auto = id_autoP and actual_return is null into data_returnP;
		perform Imposition_fine(actual_returnP, data_returnP, id_hireP,  id_autoP);
		end if;
	UPDATE Hire SET actual_return = actual_returnP where id_auto = id_autoP and actual_return is null;
	UPDATE Autos SET status = 'free' where id_auto = id_autoP;
	return 0;
END;
$$ LANGUAGE plpgsql;

/*Определение стоимости штрафа при просрочке*/
CREATE OR REPLACE FUNCTION Imposition_fine(actual_returnP date, data_returnP date, id_hireP int, id_autoP int) returns int
AS  $$
DECLARE cost_hire int;
BEGIN
	select cost_hire_in_day from autos where id_auto = id_autoP into cost_hire;
	INSERT INTO Fine_clients(id_hire, cause_fine, cost_fine) values (id_hireP, 'Просрочка по дате', 3 * cost_hire * (actual_returnP - data_returnP));
	return 0;
END;
$$ LANGUAGE plpgsql;


/*Добавление проката*/
--select add_hire(4,3, '2018-05-20', '2018-05-21', '2018-05-22');
CREATE OR REPLACE FUNCTION Add_hire(id_autoP int, id_clientP int, data_issueP date, data_returnP date) returns int
AS $$
DECLARE new_id_client int;
DECLARE new_id_auto int;
DECLARE idHire int;
	BEGIN
		IF EXISTS (SELECT id_auto, status, passport from Autos, Clients where id_auto = id_autoP and id_client = id_clientP and status ='free') then
			SELECT id_auto FROM autos WHERE id_auto = id_autoP into new_id_auto;
			SELECT id_client from clients where id_client = id_clientP into new_id_client;
			UPDATE Autos SET status = 'busy'  where id_auto = new_id_auto;
			INSERT INTO Hire (id_auto, id_client, data_issue, data_return,  total_cost)
				VALUES (id_autoP, new_id_client, data_issueP, data_returnP, Pricing(id_autoP, data_issueP, data_returnP))
				returning id_hire into idHire;
			--if (actual_returnP > data_returnP) then perform Imposition_fine(actual_returnP, data_returnP, idHire, id_autoP); end if;
			return idHire;
		else raise exception 'Ошибка! Проверьте корректность введенных данных';	
		end if;
	return 0;		
	END;
$$ LANGUAGE plpgsql;


create table images (
	id_image serial primary key not null,
	image_path oid
);
insert into images(image_path) values (lo_import('C:/PostgreSQL/9.5/scripts/images/pg-psql.ico'));
select lo_export(image_path) from images;









