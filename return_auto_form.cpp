#include "return_auto_form.h"
#include "ui_return_auto_form.h"

Return_Auto_form::Return_Auto_form(QString& id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Return_Auto_form)
{
    ui->setupUi(this);
    this->id = id;
}

Return_Auto_form::~Return_Auto_form()
{
    delete ui;
}

void Return_Auto_form::on_ReturnAutoButton_clicked()
{
    int day, month, year;
    ui->actualReturn->selectedDate().getDate(&year, &month, &day);
    QString actual_return = "\'" + QString::number(year) + "-" + QString::number(month) + "-" + QString::number(day) + "\'";
    QString strQuery = "select Return_auto(" + actual_return + "," + id + ");";
    //QMessageBox::information(this, "", strQuery);
    if (query.exec(strQuery)) {
            QMessageBox::information(this, "","Изменения внесены!");
            this->close();
    } else {
        QMessageBox::information(this, "","Непредвиденная ошибка");
    }
}
