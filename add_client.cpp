#include "add_client.h"
#include "ui_add_client.h"
#include <QMessageBox>
Add_client::Add_client(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Add_client)
{
    ui->setupUi(this);
    ui->PassportLine->setValidator(new QIntValidator(0, 999999999, this));
    ui->phoneLine->setValidator(new QIntValidator(0, 9999999999999, this));
    ui->PravaLine->setValidator(new QIntValidator(0, 999999999, this));
}

Add_client::~Add_client()
{
    delete ui;
}

/*Добавление клиента*/
void Add_client::on_addClientButton_clicked()
{
    if (ui->LastNameLine->text() != "" &&
        ui->FirstNameLine->text() != "" &&
        ui->ThirdNameLine->text() != "" &&
        ui->phoneLine->text() != "" &&
        ui->PassportLine->text() != "" &&
        ui->PravaLine->text() != "")
    {
        QString strQuery = "INSERT INTO Clients (first_name, last_name, third_name, telephone, passport, driver_license) VALUES ('" +
                           ui->FirstNameLine->text() + "','" + ui->LastNameLine->text() + "','" +  ui->ThirdNameLine->text() + "','" +
                           "89" + ui->phoneLine->text() + "','" +   ui->PassportLine->text() + "','" +  ui->PravaLine->text() +"');";
        if (query.exec(strQuery)) {
            QMessageBox::information(0, "", "Запись добавлена");
            this->close();
        }
    } else
        QMessageBox::information(this, "", "Заполните все поля");
}
