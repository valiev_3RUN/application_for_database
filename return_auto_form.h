#ifndef RETURN_AUTO_FORM_H
#define RETURN_AUTO_FORM_H

#include <QDialog>
#include <QMessageBox>
#include <QSqlQuery>
namespace Ui {
class Return_Auto_form;
}

class Return_Auto_form : public QDialog
{
    Q_OBJECT

public:
    explicit Return_Auto_form(QString& id, QWidget *parent = 0);
    ~Return_Auto_form();

private slots:
    void on_ReturnAutoButton_clicked();

private:
    Ui::Return_Auto_form *ui;
    QString id;
    QSqlQuery query;
};

#endif // RETURN_AUTO_FORM_H
