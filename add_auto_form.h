#ifndef ADD_AUTO_FORM_H
#define ADD_AUTO_FORM_H

#include <QDialog>
#include<QMessageBox>
#include <QSqlQuery>
namespace Ui {
class add_auto_form;
}

class add_auto_form : public QDialog
{
    Q_OBJECT

public:
    explicit add_auto_form(QWidget *parent = 0);
    ~add_auto_form();

private slots:
    void on_addAutoButton_clicked();

    void on_toolButton_triggered(QAction *arg1);

    void on_toolButton_clicked();

private:
    Ui::add_auto_form *ui;
    QSqlQuery query;
    QString pathImage;
};

#endif // ADD_AUTO_FORM_H
