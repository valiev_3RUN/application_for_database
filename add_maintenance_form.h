#ifndef ADD_MAINTENANCE_FORM_H
#define ADD_MAINTENANCE_FORM_H

#include <QDialog>
#include <QMessageBox>
#include <QString>
#include <QSqlQueryModel>
#include <QSqlQuery>
namespace Ui {
class Add_maintenance_form;
}

class Add_maintenance_form : public QDialog
{
    Q_OBJECT

public:
    explicit Add_maintenance_form(QWidget *parent = 0);
    ~Add_maintenance_form();

private slots:

    void on_Add_maintenance_button_clicked();

    void on_idAEdit_2_selectionChanged();

    void on_tempTableView_doubleClicked(const QModelIndex &index);

private:
    Ui::Add_maintenance_form *ui;
    QSqlQuery query;
    QSqlQueryModel qModel;
};

#endif // ADD_MAINTENANCE_FORM_H
