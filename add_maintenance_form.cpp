#include "add_maintenance_form.h"
#include "ui_add_maintenance_form.h"
#include <QSqlError>
Add_maintenance_form::Add_maintenance_form(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Add_maintenance_form)
{
    ui->setupUi(this);
}

Add_maintenance_form::~Add_maintenance_form()
{
    delete ui;
}

/*Кнопка добавления информации о ТО автомобиля*/
void Add_maintenance_form::on_Add_maintenance_button_clicked()
{
    if (ui->idAEdit_2->text() != "Double click.." && ui->brakeEdit->text() != "" && ui->oil_edit->text() != "") {
        QString dateTO = ui->dateMaintenance->text();
        dateTO = QString(dateTO.at(6)) + QString(dateTO.at(7)) + QString(dateTO.at(8)) + QString(dateTO.at(9)) + "-" + QString(dateTO.at(3)) + QString(dateTO.at(4)) + "-" + QString(dateTO.at(0)) + QString(dateTO.at(1));
        QString stringQuery = "INSERT INTO Maintenance (id_auto, oil_level, brakes, date_maintenance) values(" + ui->idAEdit_2->text() +",'" + ui->oil_edit->text() + "','" +ui->brakeEdit->text() + "','" + dateTO + "');";
        if (query.exec(stringQuery)) {
            QMessageBox::information(this, "Good", "Информация о ТО добавлена");
            this->close();
        } else {
            QMessageBox::information(this, "Ошибка добавления", query.lastError().text());
        }
    } else {
        QMessageBox::information(this, "Невозможно добавить", "Заполните все поня");
    }
}

void Add_maintenance_form::on_idAEdit_2_selectionChanged()
{
    //qModel.setQuery("Select id_auto as \"ID\", oil_level as \"Уровень масла\", brakes as \"Уровень тормозной жидеости\", date_maintenance as \"Дата ТО\" from Maintenance;");
    qModel.setQuery("Select id_auto as \"ID\", Model as \"Модель\", mark as \"Марка\", Color as \"Марка\", cost_hire_in_day as \"Цена за сутки\", Count_maintenance as \"Кол-во ТО\", status as \"Статус\" from Autos");
    ui->tempTableView->setModel(&qModel);
}

void Add_maintenance_form::on_tempTableView_doubleClicked(const QModelIndex &index)
{
    if (ui->tempTableView->currentIndex().row() >= 0) {
        QString ID;
        ID = ui->tempTableView->model()->data(ui->tempTableView->model()->index(index.row(),0)).toString();
        ui->idAEdit_2->setText(ID);
      }
}
